package main

import (
	"unsafe"
)

// main is required for TinyGo to compile to Wasm.
func main() {}

//export helloWorld
func helloWorld(bufferPosition *uint32, length int) uint64 {
	
	nameBytes := readBufferFromMemory(bufferPosition, length)
	
	message := "👋 Hello World " + string(nameBytes) + " 🌍"
	
	return copyBufferToMemory([]byte(message))
}

// readBufferFromMemory returns a buffer from the WebAssembly memory buffer
func readBufferFromMemory(bufferPosition *uint32, length int) []byte {
	buffer := make([]byte, length)
	pointer := uintptr(unsafe.Pointer(bufferPosition))
	for i := 0; i < length; i++ {
		s := *(*int32)(unsafe.Pointer(pointer + uintptr(i)))
		buffer[i] = byte(s)
	}
	return buffer
}

// copyBufferToMemory copies a buffer to the WebAssembly memory buffer
func copyBufferToMemory(buffer []byte) uint64 {
	bufferPtr := &buffer[0]
	unsafePtr := uintptr(unsafe.Pointer(bufferPtr))

	ptr := uint32(unsafePtr)
	size := uint32(len(buffer))

	return (uint64(ptr) << uint64(32)) | uint64(size)
}
