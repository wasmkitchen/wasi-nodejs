"use strict";

import * as fs from 'fs'
import { WASI } from 'wasi'

const wasi = new WASI()
const importObject = { wasi_snapshot_preview1: wasi.wasiImport };


(async () => {
  const wasm = await WebAssembly.compile(fs.readFileSync("./function/hello.wasm"));

  const instance = await WebAssembly.instantiate(wasm, importObject);

  wasi.start(instance);

  // 🖐 Prepare the string parameter
  const stringParameter = "Bob Morane 🤗";
  const bytes = new TextEncoder("utf8").encode(stringParameter);
  
  // The TinyGo `malloc` is automatically exported
  const ptr = instance.exports.malloc(bytes.length);
  const mem = new Uint8Array(
    instance.exports.memory.buffer, ptr, bytes.length
  );
  mem.set(bytes);


  // Call the `helloWorld` TinyGo function
  // Get a kind of pair of values
  const helloWorldPointerSize = instance.exports.helloWorld(ptr, bytes.length);

  const memory = instance.exports.memory;
  const completeBufferFromMemory = new Uint8Array(memory.buffer);

  const MASK = (2n**32n)-1n;

  // Extract the values of the pair (using the mask)
  const ptrPosition = Number(helloWorldPointerSize >> BigInt(32));
  const stringSize = Number(helloWorldPointerSize & MASK);

  console.log("🤖 Position:", ptrPosition);
  console.log("🤖 Size:", stringSize);

  // Extract the string from the memory buffer
  const extractedBuffer = completeBufferFromMemory.slice(
    ptrPosition, ptrPosition+stringSize
  );

  console.log("🤖 extractedBuffer:", extractedBuffer)
  
  // Decode the buffer
  const str = new TextDecoder("utf8").decode(extractedBuffer)

  console.log(str)

})()
